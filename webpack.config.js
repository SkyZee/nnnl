var path = require('path');
//import HtmlWebpackPlugin from 'html-webpack-plugin';

module.exports = {
  //debug: true,
  devtool: 'inline-source-map',
  //noInfo: false,
  entry: [
    //path.resolve(__dirname, 'src/index'),
    path.resolve(__dirname, 'src/main.ts') // my first TS integration attempt
  ],
  target: 'web',
  output: {
    path: path.resolve(__dirname, 'src'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    // Create HTML file that includes reference to bundled JS.
    // new HtmlWebpackPlugin({
    //   template: 'src/index.html',
    //   inject: true
    // }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/, exclude: /node_modules/, loaders: ['babel-loader']
      },
      // {
      //   test: /\.css$/, loaders: ['style', 'css']
      // },
      {
        test: /\.ts$/, exclude: /node_modules/, loaders: [
          {
            loader: 'awesome-typescript-loader',
            options: path.resolve(__dirname, 'src/tsconfig.json')
          }
        ]
      }
    ]
  }
}
